import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.*;
import java.util.List;
import java.util.stream.Stream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.tools.Tool;


public class QuestionBk extends JFrame {
    private HashMap<String, String> englishToMorseLib = new HashMap<>(), morseToEnglishLib = new HashMap<>();

    {
        //fill the morseToEnglishLib
        List<Object> values = Arrays.asList(englishToMorseLib.values().toArray());
        List<Object> keys = Arrays.asList(englishToMorseLib.values().toArray());
    }

    private String info = "Question BK\n" +
            "Note: \n" +
            "Save is Functional But not formatted / \n" +
            "Save to File Strea \n" +
            "Press space bar for auto transfer";

    public QuestionBk() {
        JTextArea englishTextArea = new JTextArea(20,20);
		JTextArea answerA = new JTextArea(20,20);
		JTextArea answerB = new JTextArea(20,20);
		JTextArea answerC = new JTextArea(20,20);
		JTextArea answerD = new JTextArea(20,20);
        JTextArea  answer = new JTextArea(20,20); 
        englishTextArea.setText("Enter Question Here");
		answerA.setText("A).");
		answerB.setText("B).");
		answerC.setText("C).");
		answerD.setText("D).");
        answer.setText("ANSWER:");
        englishTextArea.setLineWrap(true);
		answerA.setLineWrap(true);
		answerB.setLineWrap(true);
		answerC.setLineWrap(true);
		answerD.setLineWrap(true);
        answer.setLineWrap(true);
        englishTextArea.setWrapStyleWord(true);
		answerA.setWrapStyleWord(true);
		answerB.setWrapStyleWord(true);
		answerC.setWrapStyleWord(true);
		answerD.setWrapStyleWord(true);
        answer.setWrapStyleWord(true);
        englishTextArea.setMargin(new Insets(5, 5, 5,5));
		answerA.setMargin(new Insets(5,5,5,5));
		answerB.setMargin(new Insets(5,5,5,5));
		answerC.setMargin(new Insets(5,5,5,5));
		answerD.setMargin(new Insets(5,5,5,5));
        answer.setMargin(new Insets(5,5,5,5));
        JLabel englishTextLabel = new JLabel("MCQ");
        englishTextLabel.setHorizontalAlignment(SwingConstants.LEFT);
		
		
		

        JButton clear = new JButton("Clear");
        JButton add = new JButton("Add");

        JPanel englishControlPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        englishControlPanel.add(clear);
        englishControlPanel.add(add);


        JPanel englishTextPanel = new JPanel();
		JPanel answerAPanel = new JPanel();
		JPanel answerBPanel = new JPanel();
		JPanel answerCPanel = new JPanel();
		JPanel answerDPanel = new JPanel();
        JPanel answerPanel = new JPanel();
        englishTextPanel.setLayout(new BorderLayout());
		answerAPanel.setLayout(new BorderLayout());
		answerBPanel.setLayout(new BorderLayout());
		answerCPanel.setLayout(new BorderLayout());
		answerDPanel.setLayout(new BorderLayout());
        answerPanel.setLayout(new BorderLayout());
        englishTextPanel.add(englishTextLabel,BorderLayout.NORTH);
        englishTextPanel.add(new JScrollPane(englishTextArea), BorderLayout.CENTER);
		answerAPanel.add(new JScrollPane(answerA),BorderLayout.CENTER);
		answerBPanel.add(new JScrollPane(answerB), BorderLayout.CENTER);
		answerCPanel.add(new JScrollPane(answerC), BorderLayout.CENTER);
		answerDPanel.add(new JScrollPane(answerD), BorderLayout.CENTER);
        answerPanel.add(new JScrollPane(answer), BorderLayout.CENTER);
        answerPanel.add(englishControlPanel, BorderLayout.SOUTH);

        JTextArea morseTextArea = new JTextArea();
        morseTextArea.setText("PRESS CLEAR TEXT \n\n");
        morseTextArea.setLineWrap(true);
        morseTextArea.setWrapStyleWord(true);
        morseTextArea.setLineWrap(true);
        morseTextArea.setMargin(new Insets(5, 5, 5,5));
        morseTextArea.setFont(new Font("", 0, 20));

        JLabel morseTextLabel = new JLabel("DISPLAY PANEL");
        morseTextLabel.setHorizontalAlignment(SwingConstants.CENTER);


        JButton morseToEnglishBt = new JButton("Save");
        JButton clearMorseText = new JButton("Clear Text >>");

        JPanel morseControlPanel = new JPanel();
        morseControlPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        morseControlPanel.add(morseToEnglishBt);
        morseControlPanel.add(clearMorseText);

        JPanel morseTextPanel = new JPanel();
        morseTextPanel.setLayout(new BorderLayout());
        morseTextPanel.add(morseTextLabel, BorderLayout.NORTH);
        morseTextPanel.add(new JScrollPane(morseTextArea), BorderLayout.CENTER);
        morseTextPanel.add(morseControlPanel, BorderLayout.SOUTH);



		JPanel panel = new JPanel();
		panel.setSize(300,300);
        GridLayout paneTwo = new GridLayout(0,1);
		    paneTwo.setVgap(3);
			panel.setLayout(paneTwo);
			panel.add(new JScrollPane(englishTextArea), BorderLayout.CENTER);
			panel.add(answerA);
			panel.add(answerB);
			panel.add(answerC);
			panel.add(answerD);
            panel.add(answer);
			panel.add(englishControlPanel);
		JSplitPane splitPane =
                new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel, morseTextPanel);
        splitPane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        JTextArea infoTextArea = new JTextArea();
        infoTextArea.setLineWrap(true);
        infoTextArea.setWrapStyleWord(true);
        infoTextArea.setText(info);
        infoTextArea.setBackground(new Color(241,241,241));
        infoTextArea.setEditable(false);
        infoTextArea.setMargin(new Insets(5, 5, 5,5));


        JPanel infoPanel = new JPanel(new BorderLayout());
        infoPanel.add(infoTextArea, BorderLayout.CENTER);


        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(infoPanel, BorderLayout.NORTH);
        mainPanel.add(splitPane, BorderLayout.CENTER);
//        splitPane.setResizeWeight(0.5f);


        add.addActionListener((e) -> {
            String english = englishTextArea.getText().trim();
			String A = answerA.getText().trim();
			String B = answerB.getText().trim();
			String C = answerC.getText().trim();
			String D = answerD.getText().trim();
            String An = answer.getText().trim();
            morseTextArea.append(englishWordToDisplay(english,A,B,C,D,An));
        });

        morseToEnglishBt.addActionListener((e)->{
            String morse = morseTextArea.getText().trim();
			try{
				FileOutputStream out = new FileOutputStream("c://Users//Public//Documents//TestQuest.txt");
				out.write(morse.getBytes());
				out.close();
			}
			catch(IOException ex){
				System.out.println("error");
			}
        });


        morseTextArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                //when space bar is pressed (or back space) do the conversion
                if(Character.isWhitespace(e.getKeyChar()) || e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                    englishTextArea.setText(displayToEnglishWord(morseTextArea.getText()));
                }
            }
        });

        clear.addActionListener((e) -> {
            englishTextArea.setText(null);
			answerA.setText("A)");
			answerB.setText("B)");
			answerC.setText("C)");
			answerD.setText("D)");
        });

        clearMorseText.addActionListener((e) -> {
            morseTextArea.setText(null);
        });



        JFrame frame = new JFrame();
        frame.setTitle("Carbon BK");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(QuestionBk.class.getResource("/images/Icon.png")));
        frame.setLayout(new BorderLayout());
        frame.add(mainPanel, BorderLayout.CENTER);
        frame.setSize(new Dimension(800, 650));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setResizable(true);
        frame.setVisible(true);

        splitPane.setDividerLocation(frame.getWidth() / 2);
        add.doClick();

    }



    public String englishWordToDisplay(String englishWord,String A, String B, String C, String D, String An) {
        StringBuffer buffer = new StringBuffer();
        Stream.of(englishWord.split("[ \n]"));
         buffer.append(englishWord +"\n\n" + A +"\n" + B +"\n" + C+"\n" + D +"\n"+ An +"\n\n");
		 
        return buffer.toString();
    }
    

    public String displayToEnglishWord(String morseWord) {
        StringBuffer buffer = new StringBuffer();
        Stream.of(morseWord.split("[\\s\\n]"))
                .filter((s) -> s != null && !s.isEmpty())
                .forEach( s -> {
//                        System.out.println("s == " + s);
                        if(s.equalsIgnoreCase("/") || s.equalsIgnoreCase("|")) {
                            buffer.append(" ");
                        } else {
//                            String v = morseToEnglishLib.containsKey(s) ? morseToEnglishLib.get(s) : "?? ";
//                            System.out.println(s + " === " + v);
                            buffer.append((morseToEnglishLib.containsKey(s) ? morseToEnglishLib.get(s) : "?? ").toLowerCase());
                        }
                });
        return buffer.toString();
    }

    public static void main(String[] args) {
		SplashScreen run = new SplashScreen(7000);
		run.showSplash();
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }

        SwingUtilities.invokeLater(() -> {
            new QuestionBk();
        });
    }

}
